

#import "STRTMManager.h"
#import <AgoraRtmKit/AgoraRtmKit.h>

@interface STRTMManager ()<AgoraRtmDelegate>

@property(nonatomic, strong) AgoraRtmKit * kit;
@property(nonatomic, assign) AgoraRtmConnectionState loginState;
@property (nonatomic,strong) NSMutableDictionary <NSString *,NSDictionary *>*chatMsgHandleMap;
@property (nonatomic,strong) NSMutableDictionary <NSString *,NSDictionary *>*stategyHandleMap;

@end


@implementation STRTMManager (RTMJson)

+ (NSString *)dictToJsonString:(NSDictionary *)dict {
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:NULL];
    NSString * json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    return json;
}

+ (nullable NSDictionary *)jsonStringToObject:(NSString *)string {
    NSData * jsonData = [self jsonStringToData:string];
    NSDictionary * jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
    return jsonObject;
}

+ (NSData *)jsonStringToData:(NSString *)string {
    NSData * data = [string dataUsingEncoding:NSUTF8StringEncoding];
    return data;
}

@end


@implementation STRTMManager

+ (instancetype)sharedInstance {
    static STRTMManager * _shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}

static Class<STRTMDelegate> _rtmDClass = nil;
+ (void)setDelegateClass:(Class<STRTMDelegate>)cls {
    _rtmDClass = cls;
}


+ (void)loginHandle:(nullable void(^)(BOOL success))handle {
    
    NSString * token = [_rtmDClass rtmToken];
    NSString * uid = [_rtmDClass appUserId];
    if (!token || !uid) {
        assert(@"请检查【STRTMManager】+ (void)loginHandle:(nullable void(^)(BOOL success))handle;调用前是否有token 和 uid");
        return;
    }
    
    [[self sharedInstance].kit loginByToken:token user:uid completion:^(AgoraRtmLoginErrorCode errorCode) {
        
        BOOL loginSuccess = (errorCode == AgoraRtmLoginErrorOk || errorCode == AgoraRtmLoginErrorAlreadyLogin);
        
        if (loginSuccess) {
            NSLog(@"登录IM调用成功");
        } else {
            NSLog(@"%@",[NSString stringWithFormat:@"Login failed for user [%@] : Code: %zd",uid, errorCode]);
            if (errorCode == AgoraRtmLoginErrorTokenExpired) {
                [self refreshToken];
            }
        }
        
        if (handle) {handle(loginSuccess);}
    }];
    
}

//MARK: 登出IM
+ (void)logOutHandle:(nullable void(^)(BOOL success))handle {
    [[self sharedInstance].kit logoutWithCompletion:^(AgoraRtmLogoutErrorCode errorCode) {
        if (handle) {handle(errorCode != AgoraRtmLogoutErrorRejected);}
    }];
}








#pragma mark <AgoraRtmDelegate>
//显示当前用户的连接状态
- (void)rtmKit:(AgoraRtmKit *)kit connectionStateChanged:(AgoraRtmConnectionState)state reason:(AgoraRtmConnectionChangeReason)reason {
    
    self.loginState = state;
    NSString * text;
    if (state == AgoraRtmConnectionStateDisconnected) {//初始状态
        text = [NSString stringWithFormat:@"初始状态 Reason: %ld", (long)reason];
    } else if (state == AgoraRtmConnectionStateConnecting) {//登录中
        text = [NSString stringWithFormat:@"连接中 Reason: %ld", (long)reason];
    } else if (state == AgoraRtmConnectionStateReconnecting) {//重连中
        text = [NSString stringWithFormat:@"重连中 Reason: %ld", (long)reason];
    } else if (state == AgoraRtmConnectionStateAborted) {//退出登录
        text = [NSString stringWithFormat:@"离线 Reason: %ld", (long)reason];
    } else if (state == AgoraRtmConnectionStateConnected) {
        text = [NSString stringWithFormat:@"连接成功 Reason: %ld 🇨🇳🇨🇳🇨🇳🇨🇳🇨🇳", (long)reason];
    }
    
    if (reason == AgoraRtmConnectionChangeReasonTokenExpired) {
        [self.class refreshToken];
    }
    
    NSLog(@"%@",text);
}


- (void)rtmKit:(AgoraRtmKit *)kit messageReceived:(AgoraRtmMessage *)message fromPeer:(NSString*)peerId {
        
    NSString * jsonString = message.text;
    
    NSLog(@"消息原文：\n%@",jsonString);
    
    NSDictionary * getDict = [STRTMManager jsonStringToObject:jsonString];
    
    NSLog(@"消息对象：\n%@",getDict);
    
    if (getDict && [getDict isKindOfClass:[NSDictionary class]]) {
        
        NSString * messagTypeString = getDict[@"type"];
        
        if ([messagTypeString isEqualToString:@"ChatMessage"]) {
            
            NSDictionary * messageDict = getDict[@"message"];
            
            [_rtmDClass getChatMsgData:messageDict dealComplectHandle:^(id  _Nonnull msg, id  _Nonnull session) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

                    for (NSDictionary * handleMap in self.chatMsgHandleMap.allValues) {
                        NSString * chatUid = [handleMap objectForKey:@"uid"];

                        if (!chatUid || [chatUid isEqualToString:peerId]) {//所有用户的监听 || 收到监听对象发送的消息
                            STRTMGetMessageHandle handle = [handleMap objectForKey:@"handle"];
                            if (handle) {handle(msg,session);}
                        }
                    }

                });
            }];
            
        } else if ([messagTypeString isKindOfClass:[NSString class]] && messagTypeString.length) {
            
            if ([self.stategyHandleMap[messagTypeString] isKindOfClass:[NSDictionary class]]) {

                STRTMGetStategyHandle handle = self.stategyHandleMap[messagTypeString][@"handle"];
                NSString * dataKey = self.stategyHandleMap[messagTypeString][@"dataKey"];

                NSDictionary * dataDict = getDict[dataKey];
                if (handle) {handle(dataDict);}
            }
            
        }
        
    }
    
}



#pragma mark getter

- (AgoraRtmKit *)kit {
    if (!_kit) {
        _kit = [[AgoraRtmKit alloc] initWithAppId:[_rtmDClass rtmAppId] delegate:self];
    }
    return _kit;
}


- (NSMutableDictionary<NSString *,NSDictionary *> *)chatMsgHandleMap {
    if (!_chatMsgHandleMap) {
        _chatMsgHandleMap = [NSMutableDictionary new];
    }
    return _chatMsgHandleMap;
}

- (NSMutableDictionary<NSString *,NSDictionary *> *)stategyHandleMap {
    if (!_stategyHandleMap) {
        _stategyHandleMap = [NSMutableDictionary new];
    }
    return _stategyHandleMap;
}



#pragma mark 内部方法


//MARK: - - - - - - - - - - - New message handle
+ (void)addNewMessageHandle:(STRTMGetMessageHandle)newMessageHandle fromUid:(NSString *)fromUid identifer:(NSString *)identifer {
    if (!identifer) return;
    NSMutableDictionary * map = [NSMutableDictionary new];
    if (fromUid) {
        [map setObject:fromUid forKey:@"uid"];
    }
    if (newMessageHandle) {
        [map setObject:newMessageHandle forKey:@"handle"];
    }
    [[self sharedInstance].chatMsgHandleMap setObject:map forKey:identifer];
}


+ (void)removeNewMessageHandleWithIdentifer:(NSString *)identifer {
    if (identifer && [identifer isKindOfClass:[NSString class]] && identifer.length > 0) {
        [[self sharedInstance].chatMsgHandleMap removeObjectForKey:identifer];
        NSLog(@"%@",[self sharedInstance].chatMsgHandleMap);
    }
}

//MARK: - - - - - - - - - - - New stategy handle
+ (void)addStategyHandle:(STRTMGetStategyHandle)stategyHandle type:(NSString *)type dataKey:(NSString *)dataKey {
    
    if (!type) {
        return;
    }
    NSMutableDictionary * map = [NSMutableDictionary new];
    if (dataKey) {[map setObject:dataKey forKey:@"dataKey"];}
    if (stategyHandle) {[map setObject:stategyHandle forKey:@"handle"];}
    [[self sharedInstance].stategyHandleMap setObject:map forKey:type];
}


+ (void)removeStateHandleWithType:(NSString *)type {
    if (type && [type isKindOfClass:[NSString class]] && type.length > 0) {
        [[self sharedInstance].stategyHandleMap removeObjectForKey:type];
    }
}

///
+ (void)refreshToken {
    
    [_rtmDClass rtmTokenOutTime:^{
        [self loginHandle:nil];
    }];
    
}

+ (void)allReadMarkToTid:(NSString *)tid {
    
    NSString * uid = [_rtmDClass appUserId];
//
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    [dict setObject:@"AllRead" forKey:@"type"];
    [dict setObject:uid forKey:@"userId"];

    NSString * jsonText = [self dictToJsonString:dict];
//
    AgoraRtmMessage * msg = [[AgoraRtmMessage alloc] initWithText:jsonText];
    [[STRTMManager sharedInstance].kit sendMessage:msg toPeer:tid completion:^(AgoraRtmSendPeerMessageErrorCode errorCode) {
        if (errorCode == AgoraRtmSendPeerMessageErrorOk) {
            NSLog(@"发送已读回执");
        } else {
            NSLog(@"发送回执 - 失败 %zd",errorCode);
        }
    }];
    
    
//    [[STRTMManager sharedInstance].kit sendMessage:[[AgoraRtmMessage alloc] initWithText:jsonText] toPeer:tid completion:^(AgoraRtmSendPeerMessageErrorCode errorCode) {
//
//        NSString * text;
//            if (errorCode == AgoraRtmSendPeerMessageErrorOk)
//            {
//                text = [NSString stringWithFormat:@"Message sent from user: %@ to user: %@ content: %@", uid, tid, jsonText];
//            }
//            else
//            {
//                text = [NSString stringWithFormat:@"Message failed to send from user: %@ to user: %@ content: %@ Error: %ld", uid, tid, jsonText, (long)errorCode];
//            }
//
//        NSLog(@"%@",text);
//
//        }];
    
}



/////发送已读回执
//+ (void)sendReceiverIsReadToChatUserId:(NSInteger)chatUid {
//    if (!chatUid) {return;}
//
//    CustomMsgModel * model = [CustomMsgModel modelWithType:CustomMsgModelType_ReceiverIsRead];
//    NSString * dealMsg = [model yy_modelToJSONString];
//
//    NSString * peerId = [NSString stringWithFormat:@"%zd",chatUid];
//    [[ChatManager manager].kit sendMessage:[[AgoraRtmMessage alloc] initWithText:dealMsg] toPeer:peerId completion:^(AgoraRtmSendPeerMessageErrorCode errorCode) {
//        [ModelDebugConfig logInfo:@"发送已读回执" type:(ModelDebugLogInfoType_Step)];
//    }];
//
//    NSString* where = [NSString stringWithFormat:@"set %@=%@ where %@=%@",bg_sqlKey(@"is_read"),bg_sqlValue(@(YES)),bg_sqlKey(@"is_read"),bg_sqlValue(@(NO))];
//    [MsgItemModel bg_update:[MsgItemModel tableNameWithUid:chatUid] where:where];
//
//    NSString* sessionWhere = [NSString stringWithFormat:@"set %@=%@ where %@=%@",bg_sqlKey(@"unreadCount"),bg_sqlValue(@(0)),bg_sqlKey(@"session_Chat_user_id"),bg_sqlValue(@(chatUid))];
//    [SessionModel bg_update:[SessionModel sessionTableName] where:sessionWhere];
//
//    [ChatManager checkUnreadMsg];
//
//}
//
//



@end
