
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^STRTMHandle)(void);
typedef void(^STRTMGetStategyHandle)(NSDictionary * _Nonnull data);
typedef void(^STRTMGetMessageHandle)(id message,id session);



@protocol STRTMDelegate <NSObject>

///声网Appid
+ (NSString *)rtmAppId;

///登录RTM Token
+ (NSString *)rtmToken;

///登录用户id
+ (NSString *)appUserId;

///token 过期
+ (void)rtmTokenOutTime:(nullable void(^)(void))refresh;

+ (void)getChatMsgData:(NSDictionary *)data dealComplectHandle:(STRTMGetMessageHandle)complect;


@end




@interface STRTMManager : NSObject<STRTMDelegate>

+ (void)setDelegateClass:(Class<STRTMDelegate>)cls;

///登录
+ (void)loginHandle:(nullable void(^)(BOOL success))handle;

///登出
+ (void)logOutHandle:(nullable void(^)(BOOL success))handle;


/////监听收到消息  fromUid为消息发送者  若fromUid为空 则监听所有收到的消息
+ (void)addNewMessageHandle:(STRTMGetMessageHandle)newMessageHandle fromUid:(nullable NSString *)fromUid identifer:(NSString *)identifer;

///删除消息监听
+ (void)removeNewMessageHandleWithIdentifer:(NSString *)identifer;


///监听收到的后台自定义消息
+ (void)addStategyHandle:(STRTMGetStategyHandle)stategyHandle type:(NSString *)type dataKey:(NSString *)dataKey;

///删除对后台消息的监听
+ (void)removeStateHandleWithType:(NSString *)type;


//+ (void)allReadMarkToTid:(NSString *)tid;

@end

NS_ASSUME_NONNULL_END
