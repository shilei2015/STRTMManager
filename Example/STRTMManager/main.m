//
//  main.m
//  STRTMManager
//
//  Created by shilei2015 on 07/17/2023.
//  Copyright (c) 2023 shilei2015. All rights reserved.
//

@import UIKit;
#import "STAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STAppDelegate class]));
    }
}
