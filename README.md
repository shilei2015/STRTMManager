# STRTMManager

[![CI Status](https://img.shields.io/travis/shilei2015/STRTMManager.svg?style=flat)](https://travis-ci.org/shilei2015/STRTMManager)
[![Version](https://img.shields.io/cocoapods/v/STRTMManager.svg?style=flat)](https://cocoapods.org/pods/STRTMManager)
[![License](https://img.shields.io/cocoapods/l/STRTMManager.svg?style=flat)](https://cocoapods.org/pods/STRTMManager)
[![Platform](https://img.shields.io/cocoapods/p/STRTMManager.svg?style=flat)](https://cocoapods.org/pods/STRTMManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

STRTMManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'STRTMManager'
```

## Author

shilei2015, 244235126@qq.com

## License

STRTMManager is available under the MIT license. See the LICENSE file for more info.
